import React from 'react';
import { BrowserRouter, Route, Routes, Navigate } from 'react-router-dom';
import './styles/App.css';
import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import CreateCourse from './components/CreateCourse/CreateCourse';
import CourseInfo from './components/CourseInfo/CourseInfo';
import Registration from './components/Registration/Registration';
import Login from './components/Login/Login';
import getTime from './helpers/pipeDuration';
import { useSelector } from 'react-redux';

function App() {
	useSelector((state) => state.userReducer);
	const token = localStorage.getItem('token');

	return (
		<div className='container'>
			<div className='App'>
				<BrowserRouter>
					<Header />
					<Routes>
						<Route
							path='/courses/add'
							element={<CreateCourse getTime={getTime} />}
						/>
						<Route
							path='/courses'
							element={
								token ? <Courses getTime={getTime} /> : <Navigate to='/login' />
							}
						/>
						<Route path='/registration' element={<Registration />} />
						<Route path='/login' element={<Login />} />
						<Route path='/courses/:courseId' element={<CourseInfo />} />
						<Route path='*' element={<Navigate to='/courses' />} />
					</Routes>
				</BrowserRouter>
			</div>
		</div>
	);
}

export default App;
